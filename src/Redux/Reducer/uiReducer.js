import { types } from '../Types/types'

// Estado global del modal
const initialState = {
    userModalOpen:false
}

export const uiReducer = (state = initialState, action)=>{
    switch (action.type) {
        case types.uiUserOpenModal:
            return{
                ...state,
                userModalOpen:true
            }
        case types.uiUserCloseModal:
            return{
                ...state,
                userModalOpen:false
            }
        default:
            return state
    }
}

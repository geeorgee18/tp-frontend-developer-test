import {types} from '../Types/types'

// Estado global de los usuarios
const initialState = {
    userAll:undefined,
    userActive:null,
    userFollowers:0,
    userRepos:0,
    userFollowing:0,
    userSuscriptions:0,
}
export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        // Usuario Activo
        case types.userSetActiveEvent:
            return {
                ...state,
                userActive:action.payload
            }
        // Limpiar usuario activo
        case types.userClearActiveEvent:
            return {
                ...state,
                userActive:null
            }
        // Cargar usuarios
        case types.usersLoaded:
            return {
                ...state,
                userAll:[...action.payload]
            }
        // Cargar seguidores
        case types.userFollowersLoaded:
            return {
                ...state,
                userFollowers:action.payload
            }
        // Limpiar estado activo de los seguidores
        case types.userFollowersClearActiveEvent:
            return {
                ...state,
                userFollowers:0
            }
        // Cargar repositorios
        case types.userReposLoaded:
            return {
                ...state,
                userRepos:action.payload
            }
        // Limpiar estado activo de los repositorios
        case types.userReposClearActiveEvent:
            return {
                ...state,
                userRepos:0
            }
        // Cargar seguidos
        case types.userFollowingLoaded:
            return {
                ...state,
                userFollowing:action.payload
            }
        // Limpiar estado activo de los seguidos
        case types.userFollowingClearActiveEvent:
            return {
                ...state,
                userFollowing:0
            }
        // Cargar suscripciones
        case types.userSuscriptionsLoaded:
            return {
                ...state,
                userSuscriptions:action.payload
            }
        // Limpiar estado activo de las suscripciones
        case types.userSuscriptionsClearActiveEvent:
            return {
                ...state,
                userSuscriptions:0
            }
        default:
            return state
    }
}

let baseUrl = 'https://api.github.com/search/users?q='
let userURL ='https://api.github.com/users/'
// Función para obtener los usuarios de la api
export const fetchData = (endpoint, data, method='GET') => {
    const url = `${baseUrl}${endpoint}`
    
    if(method === 'GET') {
        return fetch(url)
    } else {
        return fetch(url, {
            method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}
// Función para obtener los seguidores del usuario activo de la api
export const fetchFollowers = (endpoint, data, method='GET') => {
    const url = `${userURL}${endpoint}/followers`
    
    if(method === 'GET') {
        return fetch(url)
    } else {
        return fetch(url, {
            method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}
// Función para obtener los repositorios del usuario activo de la api
export const fetchRepos = (endpoint, data, method='GET') => {
    const url = `${userURL}${endpoint}/repos`
    
    if(method === 'GET') {
        return fetch(url)
    } else {
        return fetch(url, {
            method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}
// Función para obtener los repositorios del usuario activo de la api
export const fetchFollowing = (endpoint, data, method='GET') => {
    const url = `${userURL}${endpoint}/following`
    
    if(method === 'GET') {
        return fetch(url)
    } else {
        return fetch(url, {
            method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}
// Función para obtener las suscripciones del usuario activo de la api
export const fetchSuscriptions = (endpoint, data, method='GET') => {
    const url = `${userURL}${endpoint}/subscriptions`
    
    if(method === 'GET') {
        return fetch(url)
    } else {
        return fetch(url, {
            method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}

// Types para el manejo del estado de la aplicación
export const types = {
    // types de usuarios
    userSetActiveEvent:'[user] Set user event active',
    userClearActiveEvent:'[user] Clear user event active',
    usersLoaded:'[user] Users loaded',
    userFollowersLoaded:'[user] Users followers loaded',
    userFollowersClearActiveEvent:'[user] Users followers clear active event',
    userReposLoaded:'[user] Users Repos loaded',
    userReposClearActiveEvent:'[user] Users Repos clear active event',
    userFollowingLoaded:'[user] Users Following loaded',
    userFollowingClearActiveEvent:'[user] Users Following clear active event',
    userSuscriptionsLoaded:'[user] Users Suscriptions loaded',
    userSuscriptionsClearActiveEvent:'[user] Users Suscriptions clear active event',
    // types del modal de usuarios
    uiUserOpenModal:'[ui] Open a new user modal',
    uiUserCloseModal:'[ui] Close a active user modal',

}

import { types } from '../Types/types'

export const uiOpenUserModalAction = ()=>({
    type:types.uiUserOpenModal
})
export const uiCloseUserModalAction = ()=>({
    type:types.uiUserCloseModal
})

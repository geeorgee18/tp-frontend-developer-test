import Swal from 'sweetalert2'
import { fetchData, fetchFollowers, fetchRepos, fetchFollowing, fetchSuscriptions } from '../Helper/fetchHelper'
import { types } from '../Types/types'

// Acción para activar el estado activo del usuario
export const userSetActiveAction = (activeUser) =>({
    type:types.userSetActiveEvent,
    payload:activeUser
}) 
// Acción para limpiar el estado activo del usuario 
export const userClearActiveAction = ()=>({
    type:types.userClearActiveEvent,
})
// Acción para mostrar las usuarios de acuerdo al nombre ingresado
export const usersLoadedAction = (userName)=>{
    return async (dispatch)=>{
        try {
            const response = await fetchData(userName)
            const body = await response.json()
            const userFetchData = body.items
            dispatch(userLoaded(userFetchData))
        } catch (error) {
            Swal.fire('Error', error, 'error')
        }
    }
}
const userLoaded = (user)=>({
    type:types.usersLoaded,
    payload:user
})
// Acción para mostrar los seguidores del usuario activo
export const usersFollowersLoadedAction = (activeUser)=>{
    return async (dispatch)=>{
        try {
            const response = await fetchFollowers(activeUser)
            const body = await response.json()
            const followersCount = body.length
            dispatch(userFollowersLoaded(followersCount))
        } catch (error) {
            Swal.fire('Error', error, 'error')
        }
    }
}
const userFollowersLoaded = (follower)=>({
    type:types.userFollowersLoaded,
    payload:follower
})
// Acción para limpiar el estado activo de los seguidores
export const userFollowersClearActiveAction = ()=>({
    type:types.userFollowersClearActiveEvent,
})
// Acción para mostrar los repositorios del usuario activo
export const usersReposLoadedAction = (activeUser)=>{
    return async (dispatch)=>{
        try {
            const response = await fetchRepos(activeUser)
            const body = await response.json()
            const reposCount = body.length
            dispatch(userReposLoaded(reposCount))
        } catch (error) {
            Swal.fire('Error', error, 'error')
        }
    }
}
const userReposLoaded = (repos)=>({
    type:types.userReposLoaded,
    payload:repos
})
// Acción para limpiar el estado activo de los seguidores
export const userReposClearActiveAction = ()=>({
    type:types.userReposClearActiveEvent,
})
// Acción para mostrar los seguidos del usuario activo
export const usersFollowingLoadedAction = (activeUser)=>{
    return async (dispatch)=>{
        try {
            const response = await fetchFollowing(activeUser)
            const body = await response.json()
            const followingCount = body.length
            dispatch(userFollowingLoaded(followingCount))
        } catch (error) {
            Swal.fire('Error', error, 'error')
        }
    }
}
const userFollowingLoaded = (following)=>({
    type:types.userFollowingLoaded,
    payload:following
})
// Acción para limpiar el estado activo de los seguidos
export const userFollowingClearActiveAction = ()=>({
    type:types.userFollowingClearActiveEvent,
})
// Acción para mostrar las suscripciones del usuario activo
export const usersSuscriptionsLoadedAction = (activeUser)=>{
    return async (dispatch)=>{
        try {
            const response = await fetchSuscriptions(activeUser)
            const body = await response.json()
            const suscriptionsCount = body.length
            dispatch(userSuscriptionsLoaded(suscriptionsCount))
        } catch (error) {
            Swal.fire('Error', error, 'error')
        }
    }
}
const userSuscriptionsLoaded = (suscriptions)=>({
    type:types.userSuscriptionsLoaded,
    payload:suscriptions
})
// Acción para limpiar el estado activo de los suscripciones
export const userSuscriptionsClearActiveAction = ()=>({
    type:types.userSuscriptionsClearActiveEvent,
})
// Importación de librerías y componentes
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Ejercicio1 } from './pages/Ejercicio1'
import { Ejercicio2 } from './pages/Ejercicio2'
import { Ejercicio3 } from './pages/Ejercicio3'
import { Header, Drawer } from './pages/Header'
import { Home } from './pages/Home'

export const App = ()=> {
  return (
    <BrowserRouter>
    <Header/>
    <Drawer/>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/ejercicio1" element={<Ejercicio1 />} />
        <Route path="/ejercicio2" element={<Ejercicio2 />} />
        <Route path="/ejercicio3" element={<Ejercicio3 />} />
      </Routes>
    </BrowserRouter>
  )
}
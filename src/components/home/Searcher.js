import { FilterContainer } from "../global/Container"
import { InputField, SearcherForm, SubmitButton } from "../global/Form"

export const Searcher = (props)=>{
    return (
        <FilterContainer>
            <SearcherForm>
                <InputField
                    labelTitle={props.labelTitle}
                    placeholder={props.placeholder}
                    onChange={props.onChangeInput}
                    defaultValue={props.defaultInputValue}/>
                <SubmitButton
                    value={props.submitValue}
                    onClick={props.submitOnClick}
                />
            </SearcherForm>
        </FilterContainer>
    )
}
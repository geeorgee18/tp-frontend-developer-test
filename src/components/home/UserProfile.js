import { UserProfileCard } from "../global/Profile"

export const UserProfile = (props) => {
    return (
        <UserProfileCard 
            id={props.id}
            login={props.login}/>
    )
}
            
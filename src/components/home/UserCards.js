import { UserCard } from "../global/Card"
import { CardContainer } from "../global/Container"

export const UserCards = (props) => {
    return (
        <CardContainer className='user-cards'>
            {props.users.slice(0, 10).map((user, index) => {
                return (
                    <UserCard 
                        key={index} 
                        user={user} 
                        onClick={()=>props.onClick(user)}/>
                )
            }
            )}
        </CardContainer>
    )
}
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import {FcTimeline} from 'react-icons/fc'
import { devices } from '../global/BreakPoints'
import { Text } from '../global/Text'
import { NavItem } from './Header'

const DrawerMenuContainer = styled.div`
    border-radius: 1em 1em 1em 1em;
    display: grid;
    justify-items: start;
    align-content: start;
    overflow-x: hidden;
    width:95vw;
    height: auto;
    margin: 0 auto;
    transition: all 1s ease-in-out;
    z-index: 99;
    @media only screen and ${devices.phones} {
        /* background-color: darkmagenta; */
        padding-left: 0.7em;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: darkgrey; */
        padding-left: 1.2em;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: darkgreen; */
        padding-left: 0.1em;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: darkred; */
        max-width:1280px;
    }
    overflow-y: scroll;
    overflow-x: hidden;
    &::-webkit-scrollbar{
        background-color: transparent;
        border-radius: 1em;
        width: 10px;
    }
    &::-webkit-scrollbar-thumb{
        background-color: #F5F5F5;
        border-radius:0.2em;
    }
`
const DrawerMenuIcon = styled.label`
    cursor: pointer;
    font-size: 1.5rem;
    font-weight: 900;
    word-spacing: 140%;
    letter-spacing: 4px;
    z-index: 99;
    position: absolute;
    @media only screen and ${devices.phones} {
        /* background-color: darkmagenta; */
        margin-top: 0.9em;
        margin-left: 0em;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: darkgrey; */
        margin-top: 1.4em;
        margin-left: 0.5em;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: darkgreen; */
        margin-top: 0.5em;
        margin-left: 0.5em;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: darkred; */
        margin-top: 0.5em;
        margin-left: 0.5em;
    }
`
const DrawerMenuInput = styled.input``
const DrawerMenu = styled.aside`
    position: fixed;
    z-index: 99;
    width: 100%;
    height: auto;
    margin-top: 0em;
    /* bottom: 0; */
    transform: translateX(-100%);
    transition: transform 0.5s cubic-bezier(0.4, 0.0, 0.2, 1);
    display: grid;
    grid-template-areas: 'menu overlay overlay';
    grid-template-columns: 5fr 5fr 5fr;
    @media only screen and ${devices.phones} {
        /* background-color: darkmagenta; */
    }
    @media only screen and ${devices.tablet} {
        /* background-color: darkgrey; */
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: darkgreen; */
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: darkred; */
    }
`
const Menu = styled.nav`
    /* glassmorphism efecto */
    background: rgba(255, 255, 255, 0.3);
    box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
    backdrop-filter: blur(2px);
    -webkit-backdrop-filter: blur(2px);
    border: 1px solid rgba(255, 255, 255, 0.3);
    /* glassmorphism efecto */
    display: block;
    flex-flow: column wrap;
    transform: translateX(-30%);
    opacity: 0;
    color: #fff;
    transition: all 500ms cubic-bezier(0.4, 0.0, 0.2, 1);
    transition-delay: 0;
    contain: content;
    grid-area: 'menu';
    box-sizing: border-box;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    border-radius: 1em 0;
    @media only screen and ${devices.phones} {
        /* background-color: darkmagenta; */
        padding: 1em;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: darkgrey; */
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: darkgreen; */
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: darkred; */
    }
`
const MenuOverlay = styled.label`
    display: block;
    grid-area: 'overlay';
    width: 100vw;
`
export const DrawerComponent = ({dataMenu}) => {
    return(
        <DrawerMenuContainer>
        <DrawerMenuIcon 
            htmlFor="menu-opener" 
            tabindex="0" 
            aria-haspopup="true" 
            role="button"	
            aria-controls="menu" 
            id="openmenu">
            <FcTimeline
                size={40}/>
        </DrawerMenuIcon>
        <DrawerMenuInput type="checkbox" data-menu id="menu-opener" hidden/>
        <DrawerMenu 
            className="DrawerMenu" 
            role="menu" 
            id="menu" 
            aria-labelledby="openmenu">
        <Menu className="Menu">
            <Text text='Menú lateral' padding='0.5em 0' color='#30336b'/>
            {
                dataMenu.map((data)=> (
                    <NavItem key={data.id}>
                        <NavLink className={({isActive}) => isActive ? 'activeNavItem': ''} to={data.navigateTo}>
                            {data.title}
                        </NavLink>
                    </NavItem>
                ))
            }
        </Menu>
        <MenuOverlay htmlFor='menu-opener'></MenuOverlay>
        </DrawerMenu>
        </DrawerMenuContainer>
    )
}
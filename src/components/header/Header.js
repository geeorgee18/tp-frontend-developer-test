import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { devices } from '../global/BreakPoints'

// Estilos Css
const HeaderElement = styled.div`
    background-color: whitesmoke;
    display: grid;
    justify-content: center;
    align-content: center;
    height: 5em;
    transition: all 1s ease-in-out;
    margin-bottom: 2em;
    width: 100vw;
    @media only screen and ${devices.phones} {
        /* background-color: darkmagenta; */
        overflow-y: hidden;
        column-gap: 2em;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: darkgrey; */
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: darkgreen; */
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: darkred; */
    }
`
const Navbar = styled.nav`
    /* display: grid;
    align-content: start;
    justify-content: start; */
`
const NavList = styled.ul`
    list-style:none;
    display: grid;
    grid-auto-flow:column;
    height: fit-content;
    justify-items:end;
    align-content:center;
    height: 100%;
    @media only screen and ${devices.phones} {
        /* background-color: darkmagenta; */
        column-gap:1em;
        font-size: 1.4em;
        padding: 0 2em;
        :nth-child(1){
            margin-left: 4em;
        }
    }
    @media only screen and ${devices.tablet} {
        /* background-color: darkgrey; */
        column-gap:6em;
        font-size: 1.4em;
        :nth-child(1){
            /* margin-left: 0em; */
        }
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: darkgreen; */
        column-gap:3em;
        font-size: 1.4em;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: darkred; */
        column-gap:4em;
        font-size: 1.6em;
    }
    `
export const NavItem = styled.li`
    cursor:pointer;
    list-style: none;
    @media only screen and ${devices.phones} {
        /* background-color: darkmagenta; */
    }
    @media only screen and ${devices.tablet} {
        /* background-color: darkgrey; */
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: darkgreen; */

    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: darkred; */
    }
`
export const HeaderComponent = ({dataMenu}) => {
    return(
        <HeaderElement>
            <Navbar>
                <NavList>
                    {
                        dataMenu.map((data)=> (
                            <NavItem key={data.id}>
                                <NavLink className={({isActive}) => isActive ? 'activeNavItem': ''} to={data.navigateTo}>
                                    {data.title}
                                </NavLink>
                            </NavItem>
                        ))
                    }
                </NavList>
            </Navbar>
        </HeaderElement>
    )
}

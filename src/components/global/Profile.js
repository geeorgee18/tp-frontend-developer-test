import styled from 'styled-components'
import { Text } from './Text'
import {GiFloorHatch} from 'react-icons/gi'
import Chart from 'react-google-charts'
import { useSelector } from 'react-redux'

const UserProfileComponent = styled.div`
    /* background-color: orangered; */
    border-radius: 0.4em;
    display: grid;
    justify-content: start;
    align-content: center;
    width: 27.4em;
    height: auto; 
    grid-template-areas: 'Icon User'
                        'ChartTitle ChartTitle'
                        'Chart Chart';
    `
const UserProfileIconContainer = styled.div`
    /* background-color: yellowgreen; */
    grid-area: Icon;
    display: inline-flex;
    gap: 0.5em;
    justify-items: center;
    align-items: center;
    width: 12.5em;
    padding-left: 0.5em;
    `
const UserProfileNameContainer = styled.div`
    /* background-color: rebeccapurple; */
    grid-area: User;
    display: grid;
    grid-template-columns: repeat(1, 1fr);
    justify-items: center;
    align-items: center;
    row-gap: 0.5em;
    column-gap: 1em;
    width: 14.9em;
`
const UserFollowersChartContainer = styled.div`
    background-color: #D6207C;
    grid-area: Chart;
    display: grid;
    justify-items: center;
    align-items: center;
    width: 27.4em;
`
const UserFollowersChartTitleContainer = styled.div`
    background-color: white;
    border-radius: 0.5em 0.5em 0em 0em;
    grid-area: ChartTitle;
    display: grid;
    justify-items: center;
    align-items: center;
    width: 27.4em;
    padding: 0.5em 1em;
`
// Configuración de grafico de seguidores
export const options = {
    // title: "Número de Seguidores, Repositorios, Siguiendo y Suscriptores",
    chartArea: { width: "40%", height: "80%" },
    hAxis: {
        title: "Números",
        minValue: 0,
    },
        vAxis: {
        title: "Datos del Usuario",
    },
}
export const UserProfileCard = (props) => {
    // Llamado del hook useSelector para obtener el estado
    const { 
        userFollowers, 
        userRepos, 
        userActive,
        userFollowing,
        userSuscriptions } = useSelector(state => state.user)

    // Datos del gráfico
    const data = [
        ["Números", "Total"],
        ["Seguidores", userFollowers],
        ["Repositorios", userRepos],
        ["Seguidos", userFollowing],
        ["Suscripciones", userSuscriptions],
    ]

    return (
        <UserProfileComponent>
            <UserProfileIconContainer>
                <GiFloorHatch size={64} color='#30336b'/>
                <Text text={props.id} color='#30336b' fontSize='1.5em'/>
            </UserProfileIconContainer>
            <UserProfileNameContainer>
                <Text text={`Usuario: ${props.login}`} color='#30336b' fontSize='1.5em'/>
            </UserProfileNameContainer>
            <UserFollowersChartTitleContainer>
                <Text 
                    text={`Número de Seguidores, Repositorios, Siguiendo y Suscriptores`} 
                    color='#30336b' 
                    fontSize='1.2em'/>
            </UserFollowersChartTitleContainer>
            <UserFollowersChartContainer>
                {
                    userActive &&
                        <Chart
                            chartType="BarChart"
                            width="100%"
                            height="400px"
                            data={data}
                            options={options}
                        />
                }
            </UserFollowersChartContainer>
        </UserProfileComponent>
    )
}
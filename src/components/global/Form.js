import styled, { keyframes } from 'styled-components'
import { devices } from './BreakPoints'

// Formulario
const SearcherFormComponent = styled.form`
    /* background-color: rebeccapurple; */
    display: grid;
    align-content: center;
    justify-items: center;
    grid-gap: 1em;
    padding: 0;
    @media only screen and ${devices.phones} {
        /* background-color: pink; */
        width:100%;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        width:90vw;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        width:95vw;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2980b9; */
        max-width:1280px;
    }
`
export const SearcherForm = (props) => {
    return (
        <SearcherFormComponent>
            {props.children}
        </SearcherFormComponent>
    )
}
// Label
const LabelComponent = styled.label`
    display: block;
    font-size: 1.5em;
    color: #2d3436;
    font-weight: bold;
    margin-bottom: 0.5em;
    text-align: center;
    @media only screen and ${devices.phones} {
        /* background-color: pink; */
        width: 90%;
        margin: 0 auto;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        width: 22em;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        width: 22em;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2980b9; */
        width: 22em;
    }
`
const Label = (props) => {
    return (
        <LabelComponent>
            {props.title}
        </LabelComponent>
    )
}
// Input
const Input = styled.input`
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 5px;
    color: #2c3e50;
    font-size: 1.5em;
    padding: 0 0 0 0.5em;
    height: 40px;
    ::placeholder {
        opacity: 0.5;
    }
    @media only screen and ${devices.phones} {
        /* background-color: pink; */
        width: 90%;
        height: 2.2em;
        margin: 0 auto;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        width: 22em;
        height: 2.1em;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        width: 22em;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2980b9; */
        width: 22em;
    }
`
export const InputField =(props)=>{
    return(
        <>
            <Label title={props.labelTitle}/>
            <Input
                type='text' 
                placeholder={props.placeholder}
                onChange={props.onChange}
                defaultValue={props.defaultValue}/>
        </>
    )
}
// Select
const Select = styled.select`
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 5px;
    color: #2c3e50;
    font-size: 0-8em;
    padding: 0 0 0 0.5em;
    width: 530px;
    height: 40px;
`
const Option = styled.option`
    color: #2c3e50;
    font-size: 1em;
    padding: 0;
    text-align: center;
`
// Submit 
const pulse = keyframes`
    0% {
    transform: translateY(3px);
    } 
    50% {
        transform: translateY(1px);
    }
    100% {
        transform: translateY(3px);
    }
`
const Submit = styled.input`
    background-color: rgba(245, 39, 145, 0.025);
    border-radius: 5px;
    border: solid 2px #30336b;
    color: #30336b;
    font-weight: 900;
    font-size: 1.5em;
    width: 530px;
    height: 50px;
    cursor: pointer;
    &:hover{
        background-color: rgba(255, 255, 255, 0.25);
        animation: ${pulse} 1s infinite; 
    }
    @media only screen and ${devices.phones} {
        /* background-color: pink; */
        width: 90%;
        height: 2.2em;
        margin: 0 auto;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        width: 22em;
        height: 2.1em;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        width: 22em;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2980b9; */
        width: 22em;
    }
`
export const SubmitButton = (props) => {
    return (
        <Submit
            type='submit'
            value={props.value}
            onClick={props.onClick}
        />
    )
}
export const SelectList =(props)=>{
    return(
        <>
            <Label title={props.labelTitle}/>
            <Select onChange={props.onChange}>
                <Option value="">{props.title}</Option>
                {
                    props.dataMap.map((object,index) =>(
                            <Option key={index} value={object.value}>
                                {object.title}
                            </Option>
                        ))
                }
            </Select>
        </>
    )
}
import parse from 'html-react-parser'
import styled from 'styled-components'
import { devices } from './BreakPoints'

const TitlePageComponent = styled.h1`
    padding: 1em;
    @media only screen and ${devices.phones} {
        /* background-color: pink; */
        width:90vw;
        text-align: center;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        width:90vw;
        text-align: center;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        width:95vw;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2980b9; */
        max-width:1280px;
    }
`
export const TitlePage = (props) => {
    return (
        <TitlePageComponent>
            {props.text}
        </TitlePageComponent>
    )
}
const ParagraphComponent = styled.p`
    font-size: 1.5rem;
    margin: 1em 0;
    padding: 0 1em ;
    text-align: left;
    color: #2d3436;
    @media ${devices.phones} {
        font-size: 1.2rem;
    }
`
export const Paragraph = (props) => {
    return (
        <ParagraphComponent>
            {parse(props.text)}
        </ParagraphComponent>
    )
}

const TextCardComponent = styled.p`
    color: whitesmoke;
    font-size: 1.2em;
    display: inline-block;
    position: absolute;
    bottom:0.5em;
    left: 0.5em;
    padding: 0.4em 0.5em;
    max-width: 80%;
    max-height: 18%;
    background: #683080;
    border-radius: 0.4em;
    overflow: hidden;
`
export const TextCard = (props) => {
    return (
        <TextCardComponent>
            {props.text}
        </TextCardComponent>
    )
}
const TextComponent = styled.p`
    color: ${props => props.color ? props.color : 'whitesmoke'};
    font-size: ${props => props.fontSize ? props.fontSize : '1.2em'};
    padding: ${props => props.padding ? props.padding : '0.4em 0.5em'};
`
export const Text = (props) => {
    return (
        <TextComponent color={props.color} fontSize={props.fontSize} padding={props.padding}>
            {props.text}
        </TextComponent>
    )
}
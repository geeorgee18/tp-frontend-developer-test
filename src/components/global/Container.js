import styled from 'styled-components'
import { devices } from './BreakPoints'

export const PageContainer = styled.div`
    /* glassmorphism efecto */
    background: rgba(255, 255, 255, 0.5);
    border-radius: 16px;
    box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
    backdrop-filter: blur(5px);
    -webkit-backdrop-filter: blur(5px);
    border: 1px solid rgba(255, 255, 255, 0.3);
    /* glassmorphism efecto */
    border-radius: 1em 1em 1em 1em;
    display: grid;
    justify-items: center;
    align-content: start;
    overflow-x: hidden;
    margin: 0 auto;
    transition: all 1s ease-in-out;
    margin-bottom: 1em;
    @media only screen and ${devices.phones} {
        /* background-color: #2980b9; */
        display: block;
        width:90vw;
        height: calc(100vh - 8.5em);
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        width:90vw;
        height: calc(100vh - 9em);
        margin: 0 auto;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        width:95vw;
        height: calc(100vh - 9em);
        margin: 0 auto;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2c3e50; */
        max-width:1280px;
        width:95vw;
        height: calc(100vh - 8.5em);
    }
    overflow-y: scroll;
    overflow-x: hidden;
    &::-webkit-scrollbar{
        background-color: transparent;
        border-radius: 1em;
        width: 10px;
    }
    &::-webkit-scrollbar-thumb{
        background-color: #F5F5F5;
        border-radius:0.2em;
    }
`
export const FilterContainer = styled.div`
    /* background-color: yellowgreen; */
    display: grid;
    @media only screen and ${devices.phones} {
        /* background-color: pink; */
        width:90vw;
        height: 200px;
        margin-top: 1em;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        width:90vw;
        height: 220px;
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        width:95vw;
        height: 240px;
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2980b9; */
        max-width:1280px;
        height: 260px;
    }
`
export const CardContainer = styled.div`
    display: grid;
    width: 90%;
    height: fit-content;
    @media only screen and ${devices.phones} {
        /* background-color: #2980b9; */
        grid-template-columns: 1fr;
        margin: 0 auto;
    }
    @media only screen and ${devices.tablet} {
        /* background-color: #27ae60; */
        grid-template-columns: repeat(2, 1fr);
    }
    @media only screen and ${devices.mediumDesktop}{
        /* background-color: #e67e22; */
        grid-template-columns: repeat(3, 1fr);
    }
    @media only screen and ${devices.largeDesktop}{
        /* background-color: #2c3e50; */
        grid-template-columns: repeat(4, 1fr);
    }
`


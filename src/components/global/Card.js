import styled, {keyframes} from 'styled-components'
import { UserCardBaseButton } from './Button'
import { ImageCard } from './Image'
import { Text, TextCard } from './Text'

const startTransition = keyframes`
    0% {
        opacity: 0;
        top: 200px;
        transform: scale(0.5);
        height: 0;
    }  
    40% {
        opacity: 1;
    }
    100% {
        top: 22px;
        opacity: 1;
        transform: scale(1);
        height: 15px;
        box-shadow: 0 5px 10px -2px rgba(0, 0, 0, 0.2);
    }
`
const smoothbounceball = keyframes`
    from { transform: translate3d(0, 5px, 0);}
    to { transform: translate3d(0, -10px, 0);}
` 
const UserCardComponent = styled.div`
    /* background-color: #2c3e50; */
    border-radius: 0.4em;
    display: grid;
    justify-content: center;
    align-content: center;
    height: fit-content;  
    width: 100%;
    justify-self: center;
    `
const UserCardContent = styled.div`
        /* background-color: rebeccapurple; */
        display: grid;
        justify-items: center;
        align-items: center;
        row-gap: 1em;
        padding: 2em;
    `
const UserCardCoverTop = styled.div`
    background: #D6207C;
    position: absolute;
    left: 0;
    width: calc(100% + 64px);
    margin-left: -32px;
    z-index: 4;
    border-radius: 3px 3px 4px 4px;
    opacity: 0;
`  
const UserCardCover = styled.div`
    background: #D6207C;
    border-radius: 6px;
    width: inherit;
    height: inherit;
    position: absolute;
    overflow: hidden;
    top: 0;
    transition: 0.3s ease;
    transform-origin: top left;
    z-index: 2;
`
const UserCardWrapper = styled.div`
    width: 200px;
    height: 200px;
    position: relative;
    display: inline-block;
    perspective: 800px;
    &:hover ${UserCardCoverTop} {
        background: #952786;
        animation: ${startTransition} 0.15s forwards 0.1s;
    }
    &:hover ${UserCardCover}{
        transform: rotateX(78deg);
    }
`
const UserCardCoverBorder = styled.div`
    border-radius: 0.4em;
    border: 7px solid whitesmoke;
    background-color: whitesmoke;
    width: 80%;
    height: 80%;
    margin: 10%;
`
const UserCardBase = styled.div`
    background: #222;
    width: inherit;
    height: inherit;
    position: absolute;
    top: 0;
    border-radius: 8px;
    padding: 15px;
    grid-gap: 2em;
    display: grid;
    grid-template-columns: repeat(1, 1fr);
    grid-template-rows: repeat(1, 1fr);
    justify-items: center;
    align-items: center;
`
const UserCardIDContainer = styled.div`
    display: grid;
    justify-items: center;
    align-items: center;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: repeat(1, 1fr);
    width: 100%;
    height: 100%;
    background: #222;
    border-radius: 0.4em;
    box-shadow: 0 5px 10px -2px rgba(0, 0, 0, 0.2);
`
const UserCardBaseInfo = styled.div`
    border-radius: 4px;
    box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.9);
    background-color: #2d3436;
`
const BounceIcon = styled.div`
    font-size: 2em;
    animation: ${smoothbounceball} 1s;
    animation-direction: alternate;
    animation-iteration-count: infinite; 
`

export const UserCard = (props) => {
    return (
        <UserCardComponent>
                <UserCardContent>
                    <UserCardWrapper>
                        <UserCardCover>
                            <UserCardCoverBorder>
                                <TextCard text={props.user.login}/>
                                <ImageCard src={props.user.avatar_url}/>
                            </UserCardCoverBorder>
                        </UserCardCover>
                        <UserCardCoverTop></UserCardCoverTop>
                        <UserCardBase>
                            <UserCardBaseInfo>
                                <UserCardBaseButton
                                    onClick={props.onClick}
                                    text='Ver perfil'/>
                            </UserCardBaseInfo>
                        </UserCardBase>
                    </UserCardWrapper>
                    <UserCardIDContainer>
                        <BounceIcon>☝</BounceIcon>
                        <Text text={props.user.id}/>
                    </UserCardIDContainer>
                </UserCardContent>
        </UserCardComponent>
    )
}

import styled from 'styled-components'

const ImageCardComponent = styled.img`
    width: 100%;
    object-fit: cover;
    box-shadow: 0 0 0.5em 0.5em rgba(0, 0, 0, 0.2);
    border-radius: 0.4em;
`
export const ImageCard = (props) => {
    return (
        <ImageCardComponent src={props.src} alt={props.alt} />
    )
}
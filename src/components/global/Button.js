import styled from 'styled-components'
import { Text } from './Text'
import {GoOctoface} from 'react-icons/go'
const UserCardBaseButtonComponent = styled.button`
    background-color: #2d3436;
    border-radius: 4px;
    border: none;
    color: white;
    padding: 8px;
    font-size: 1em;
    cursor: pointer;
    transition: 0.3s ease;
    &:hover {
        background-color: #2c3e50;
    }
`
export const UserCardBaseButton = (props) => {
    return (
        <UserCardBaseButtonComponent onClick={props.onClick}>
            <GoOctoface size={40}/>
            <Text text={props.text} />
        </UserCardBaseButtonComponent>
    )
}
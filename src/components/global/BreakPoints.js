// Dimensiones para los dispositivos   
const size = {
    phones: '768px',
    tablet: '768px',
    mediumDesktop: '992px',
    largeDesktop: '1200px'
}
// Exportación de variables de tamaño
export const devices = {
    phones: `(max-width: ${size.phones})`,
    tablet: `(min-width: ${size.tablet})`,
    mediumDesktop: `(min-width: ${size.mediumDesktop})`,
    largeDesktop: `(min-width: ${size.largeDesktop})`,
};

import styled from 'styled-components'
import { devices } from '../components/global/BreakPoints'
import { PageContainer } from "../components/global/Container"
import { Paragraph, TitlePage } from "../components/global/Text"

const exerciseData ={
    text: `Si su aplicación requiere que un elemento con 
    <i>id=”elemento_dinamico” </i> se muestre solamente 
    al ubicar el cursor sobre otro elemento que tiene como 
    <i>id=”elemento”</i>, escriba un código que lo 
    permita <br/> (sea creativo en su modo de presentar el elemento que aparece, 
        recuerde que al retirar el cursor debe ocultarse el elemento).`,
}
const ContainerDinamico = styled.div`
    background: #212534;
    border-radius: 0.5em;
    height: auto;
    width: 70%;
    display: grid;
    align-items: center;
    justify-items: center;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
    margin: 0 auto;
    @media ${devices.mobileS} {
        padding: 0.5em;
    }
`
const LetrasDinamicas = styled.span`
    max-width: 0;
    opacity: 0;
    transition: 0.5s ease-in;
    color: aliceblue;
`
const ElementoDinamico = styled.div`
    /* background-color: rebeccapurple; */
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    color: white;
    font-size: 5em;
    font-weight: 900;
    cursor: pointer;
    :hover ${LetrasDinamicas}{
        opacity: 1;
        max-width: 1em;
    }
    `
const TituloDinamico = styled.h2`
    /* background-color: red; */
    font-size: 2em;
    color: whitesmoke;
    display: flex;
    &:hover ~ ${LetrasDinamicas}{
        opacity: 1;
        max-width: 1em;
    }
` 
export const Ejercicio1 = ()=> {
    
    return (
        <PageContainer>
            {/* Componente título de la página */}
            <TitlePage text="Ejercicio 1" />
            <Paragraph {...exerciseData} />
            <ContainerDinamico>
            <TituloDinamico>Elemento dinámico</TituloDinamico>
            <ElementoDinamico>
                    <span>T</span>
                    <LetrasDinamicas className="tpq">E</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">L</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">E</LetrasDinamicas>
                    <span>P</span>
                    <LetrasDinamicas className="tpq">E</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">R</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">F</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">O</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">R</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">M</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">A</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">N</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">C</LetrasDinamicas>
                    <LetrasDinamicas className="tpq">E</LetrasDinamicas>
                </ElementoDinamico>
            </ContainerDinamico>
        </PageContainer>
    )
}
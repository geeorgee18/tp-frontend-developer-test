import styled, { css }  from "styled-components"
// import { devices } from "../components/global/BreakPoints"
import { PageContainer } from "../components/global/Container"
import { Paragraph, Text, TitlePage } from "../components/global/Text"
import tpIcon from '../assets/images/teleperformance_logo.png'
import { useState } from "react"

const exerciseData ={
    text: `Escriba un ejemplo de código en JS para agregar o retirar 
    una clase “show” al realizar click en un elemento definido
    `,
}
const ContainerExercise2 = styled.div`
    background: #212534;
    border-radius: 0.5em;
    height: auto;
    width: 50%;
    display: grid;
    align-items: center;
    justify-items: center;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 2fr;
    margin: 0 auto;
`
const NavbarContainer = styled.nav`
    /* background-color: purple; */
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    width: 100%;
    height:auto;
`
const NavItem = styled.button`
    border-radius: 0.15em;
    padding: 0.2em;
    border: none;
    font-size:2em;
    margin: 1em;
    width: fit-content;
    &:hover, &:focus{
        color: darkblue;
    }
    &:active{
        color: red;
    };
`
const ShowIcon = styled.img`
    border-radius: 0.5em;
    width: 12em;
    object-fit: cover;
    opacity: ${(props) => props.show ? css`1`: css`0`};
`
export const Ejercicio2 = ()=> {
    const [showIcon, setShowIcon] = useState(false)

    return (
        <PageContainer>
            {/* Componente título de la página */}
            <TitlePage text="Ejercicio 2" />
            <Paragraph {...exerciseData} />
            <ContainerExercise2>
                <NavbarContainer>
                    <NavItem onClick={() => { setShowIcon(true) } }>
                        <Text
                            color='#2c3e50'
                            fontSize='1em'
                            padding='0'
                            text='Agregar Clase'/>
                    </NavItem> 
                    <NavItem onClick={() => { setShowIcon(false) } }>
                        <Text
                            color='#2c3e50'
                            fontSize='1em'
                            padding='0'
                            text='Remover Clase'/>
                    </NavItem>    
                </NavbarContainer>
                <ShowIcon src={tpIcon} show={showIcon}/>
            </ContainerExercise2>
        </PageContainer>
    )
}
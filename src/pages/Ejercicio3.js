import styled from 'styled-components'
import { devices } from '../components/global/BreakPoints'
import { PageContainer } from "../components/global/Container"
import { Paragraph, TitlePage } from "../components/global/Text"
const exerciseData ={
    text: `Del siguiente código identifique los errores que no 
    permitirían una correcta visualización de los elementos, 
    identifique malas prácticas y elementos que no corresponden 
    a HTML5. Realice las correcciones correspondientes`,
}
const ContainerEjercicio = styled.div`
    background: #212534;
    border-radius: 0.5em;
    height: auto;
    width: 70%;
    display: grid;
    align-items: center;
    justify-items: center;
    margin: 0 auto;
    @media ${devices.mobileS} {
        padding: 0.5em;
    }
`

export const Ejercicio3 = ()=> {
    return (
        <PageContainer>
            {/* Componente título de la página */}
            <TitlePage text="Ejercicio 3" />
            <Paragraph {...exerciseData} />
            <ContainerEjercicio>
                <div className="container-fluid">
                    <div className="row w-100">
                        <div className ="col-md-4">
                            {/* error de escritura las comillas ” y ” no son las correctas para la escritura de los valores de las propiedades*/}
                            {/* error, no se espesifican las unidades de las  dimeniones del ancho de la tarjeta */}
                            <div className="card" style={{textColor:'red', width: '100%', heigth: '200%'}}> 
                                <div className="card-header">
                                    <h3 className="card-title">
                                        <i className="fas fa-chalkboard-teacher"></i>
                                    </h3>
                                </div>
                                <div className="card-body">
                                    <div className="col-md-12">
                                        <label><b>To Update an existing user</b></label>
                                        <button type="button" className="col-12 btn ">Update</button>
                                    </div>
                                    {/*error al cerrar las etiquetas de salto de línea */}
                                    <br/> 
                                    {/*error al cerrar las etiquetas de salto de línea */}
                                    <br/>
                                    <div className="col-md-12">
                                        <label><b>To add a new user</b></label>
                                        <button type="button" className="col-12 btn bg-gradient-danger">Add New</button>
                                    </div>
                                    {/*error al cerrar las etiquetas de salto de línea */}
                                    <br/> 
                                    {/*error al cerrar las etiquetas de salto de línea */}
                                    <br/>
                                    {/*error, para el caso de esta etiqueda div esta con solo saltos de línea sin contenido */}
                                    <div className="col-md-12"> 
                                        {/*error al cerrar las etiquetas de salto de línea */}
                                        <br/> 
                                        {/*error al cerrar las etiquetas de salto de línea */}
                                        <br/>
                                    </div>
                                </div>
                                {/* error de escritura las comillas ” y ” no son las correctas para la escritura de los valores de las propiedades*/}
                                <div className="card-footer" style={{width:'50px',height:'50px'}}> 
                                </div>
                            </div>
                        </div>
                    <div className ="col-md-8">
                        <div className="card" style={{height:'100%'}}>
                            <div className="card-header">
                                <h3 className="card-title">
                                    <i className="fas fa-chalkboard-teacher"></i>
                                    Current Users
                                {/*error, se olvido cerrar la etiqueta de titulo h3  */}
                                </h3>
                            </div>
                            <div className="card-body">
                                {/*error, hay una etiqueta de tabla abierta pero nunca se cerro  */}
                                <table className="table table-striped" id="users-table"> 
                                    {/*error, se invirtio el cierre y la apertura de la etiqueta caption y no puede estar por fuera de la tabla  */}
                                    <caption style={{color:'#30336b'}}> Tabla de usuarios</caption> 
                                </table>
                                {/* no se puede asignar el atributo head a una etiqueta de tabla */}
                                <table className="thead-bluelight">
                                    {/* error, tr no puede aparecer como hibo directo de la tabla debe estar contenido en una etiqueta tbody */}
                                    <tbody>
                                        <tr>
                                            <th className="all">Code</th>
                                            <th className="all">Username</th>
                                            <th className="all">Name</th>
                                        </tr>
                                    </tbody>
                                {/* hay una etiqueta de caption de cerrado volando y no puede estar por fuera de la tabla */}
                                {/* <caption></caption> */}
                                {/*error, se agrego una propiedad no valida sobre la etiqueda de cerrado </table>, estas solo van en las de apertura*/}
                                </table> 
                                    {/* La etiqueta tbody si su contenido estan por fuera de la etiqueta table */}
                                    <table>
                                        <tbody> 
                                            {/* error, a la hora de escribir los estilos hace falta del separador ";" para asignar la siguiente propuiedad  */}
                                            {/* error de escritura las comillas ” y ” no son las correctas para la escritura de los valores de las propiedades*/}
                                            <tr style={{lineHeight: 1}}>
                                                <td style={{color:'blue',fontSize:'5px'}}>12334 </td> 
                                                <td >Fausto.1 </td>
                                                <td >Fausto</td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                            <div className="card-footer">
                            </div>
                        </div>
                    </div>
                    {/*error, hacia falta cerrar la etiqueta div*/}
                    </div>
                {/*error, hacia falta cerrar la etiqueta div*/}
                </div>
            </ContainerEjercicio>
        </PageContainer>
    )
}
import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import Swal from "sweetalert2"
import { PageContainer } from "../components/global/Container"
import { TitlePage } from "../components/global/Text"
import { Searcher } from "../components/home/Searcher"
import { UserCards } from "../components/home/UserCards"
import { uiOpenUserModalAction } from "../Redux/Actions/uiAction"
import { userSetActiveAction, usersFollowersLoadedAction, usersFollowingLoadedAction, usersLoadedAction, usersReposLoadedAction, usersSuscriptionsLoadedAction } from "../Redux/Actions/userAction"
import { UserModal } from "./Modal"


export const Home = () => {
    // Llamada del hook de redux
    const dispatch = useDispatch()
    // Llamada del hook useState
    const [userData, setUserData] = useState('')
    // Llamado del hook useSelector para obtener el estado
    const { userAll } = useSelector(state => state.user)
    // Llamada de la acción de cargar los usuarios
    const handleSubmit = (event) => {
        event.preventDefault()
        const submitData = userData.toLowerCase()
        if (submitData === 'teleperformance') {
            return(
                Swal.fire({
                    title: 'Error',
                    text: 'La busqueda por Teleperformance no es valida',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            )
        }
        else if (submitData.trim().length < 4){
            return(
                Swal.fire('Error', 'El nombre de usuario debe tener al menos 4 caracteres', 'error')
            )
        }
        dispatch(usersLoadedAction(userData))
    }
    // data del componente buscador(Searcher)
    const searcherData = {
        labelTitle:'¿Buscas algún nombre en especial?',
        placeholder:'Escribe un nombre aquí',
        submitOnClick:handleSubmit,
        defaultInputValue:userData,
        onChangeInput:(event)=>{
            setUserData(event.target.value)
        },
        submitValue:'Buscar',
    }
    // Función para abrir el modal
    const openUserModal = (user)=>{
        let userLogin = user.login
        dispatch(userSetActiveAction(user))
        dispatch(usersFollowersLoadedAction(userLogin))
        dispatch(usersReposLoadedAction(userLogin))
        dispatch(usersFollowingLoadedAction(userLogin))
        dispatch(usersSuscriptionsLoadedAction(userLogin))
        dispatch(uiOpenUserModalAction())
    }
    return (
        <PageContainer>
            {/* Componente título de la página */}
            <TitlePage text='Prueba Desarrollador Frontend TP'/>
            {/* Componente buscador */}
            <Searcher {...searcherData}/>
            {/* Componente tarjetas de usuarios */}
            {
                userAll !== undefined &&
                <UserCards users={userAll} onClick={(user)=>openUserModal(user)}/>
            }
            {/* Componente modal de usuarios */}
            <UserModal/>
        </PageContainer>
    )
}

import { DrawerComponent } from '../components/header/Drawer'
import { HeaderComponent } from '../components/header/Header'

const data =  [
    {id:0, title:'Inicio', navigateTo:'/'},
    {id:1, title:'Ejercicio1', navigateTo:'/ejercicio1'},
    {id:2, title:'Ejercicio2', navigateTo:'/ejercicio2'},
    {id:3, title:'Ejercicio3', navigateTo:'/ejercicio3'},
]

export const Header = ()=>{
    return(
        <HeaderComponent dataMenu={data}/>
    )
}
export const Drawer = ()=>{
    return(
        <DrawerComponent dataMenu={data}/>
    )
}
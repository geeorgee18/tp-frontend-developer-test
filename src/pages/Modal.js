import { useDispatch, useSelector } from 'react-redux'
import Modal from 'react-modal'
// import Swal from 'sweetalert2'
// import { useEffect, useState } from 'react'
import { uiCloseUserModalAction } from '../Redux/Actions/uiAction'
import { userClearActiveAction, userFollowersClearActiveAction, userFollowingClearActiveAction, userReposClearActiveAction, userSuscriptionsClearActiveAction } from '../Redux/Actions/userAction'
import { UserProfile } from '../components/home/UserProfile'

Modal.setAppElement('#root')
const customStyles = {
    content:{
        top:'50%',
        left:'50%',
        rigth:'auto',
        bottom:'auto',
        marginRight: '-50%',
        transform:'translate(-50%,-50%)',
        width: '30rem',
        height: 'auto',
        boxShadow:' 2px 2px 10px -9px rgba(0, 0, 0, .3)',
        background: 'rgba(255, 255, 255, 0.6)',
        backdropFilter: 'blur(5px)',
    }
}

export const UserModal = ()=>{
    const dispatch = useDispatch()
    // Llamado del hook useSelector para obtener el estado
    const { userModalOpen } = useSelector(state => state.ui)
    const { userActive } = useSelector(state => state.user)
    // Function to close Modal
    const closeModal = ()=>{
        dispatch(userClearActiveAction())
        dispatch(userFollowersClearActiveAction())
        dispatch(userReposClearActiveAction())
        dispatch(userFollowingClearActiveAction())
        dispatch(userSuscriptionsClearActiveAction())
        dispatch(uiCloseUserModalAction())
    }
    
    return(
        <Modal
            isOpen={userModalOpen}
            onRequestClose={closeModal}
            style={customStyles}
            closeTimeoutMS={200}
            overlayClassName='modal-fondo'>
                {
                    userActive !== null &&
                    <UserProfile {...userActive}/>
                }
        </Modal>
    )
}

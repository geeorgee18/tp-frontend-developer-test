# Prueba para desarrollador web Fronted TP

## Visualización del proyecto

El proyecto está conectado a través de las pipelines de gitalb hacia vercel donde se puede visualizar su funcionamiento en el siguiente [link](https://tp-frontend-developer-test.vercel.app/)

## Aclaraciones 

Solo existe un archivo global de estilos css llamada index.css que se encuentra en el directorio raíz del proyecto. Los demás estilos de los componentes se encuentran en la carpeta componente.

Tanto el menú lateral como el superior dirigen a las mismas vistas de los ejercicios de los demás componentes.

Para cerrar el menú lateral basta con dar un clic afuera de él en cualquier región hacia la derecha de este.

Están definidos los tamaños responsivos para la visualización desde diferentes dispositivos.

El ejercicio 3 está comentado dentro del código cada uno de los errores encontrados y actualmente corregidos 

## Tecnología web Utilizada

ReactJS ---> create-react-app

## Librerias utilizadas

[StyledComponents](https://styled-components.com/docs/basics#installation)

[ReactRouterDom](https://v5.reactrouter.com/web/guides/quick-start)

[react-redux](https://www.npmjs.com/package/react-redux)

[redux](https://redux.js.org/)

[redux-thunk](https://www.npmjs.com/package/redux-thunk)

[sweetalert2](https://sweetalert2.github.io/)

[react-modal](https://www.npmjs.com/package/react-modal)

[react-icons](https://react-icons.github.io/react-icons/)

[react-google-charts](https://www.react-google-charts.com/)

[html-react-parser](https://www.npmjs.com/package/html-react-parser)

[react-bootstrap](https://react-bootstrap.github.io/) -> Utilizado solamente en la sección del ejercicio 3 para ver su contenido

## Comandos de ejecución

Para su visualización de manera local se debe tener instalado NodeJS y luego puede clonar este proyecto y una vez en la raíz de este ejecutar el siguiente comando: 

### `npm install`

Luego para ejecutarlo ejecutar el siguiente:

### `npm run start`

